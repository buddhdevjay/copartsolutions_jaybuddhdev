import java.util.Scanner;

public class PieceOfCake {

    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in); // Reading from System.in
        System.out.println("Enter number of cakes and its desired areas: ");
        int noOfCakes = reader.nextInt();
        int[] cake = new int[noOfCakes];
        for (int i = 0; i < noOfCakes; i++) {
            cake[i] = reader.nextInt();
        }
        System.out.println("Output is as follows:");
        for (int i = 0; i < noOfCakes; i++) {
            System.out.println(findSmallestParameter(cake[i]));
        }

    }



    public static int findSmallestParameter(int areaDesired) {
        int minimumPerimeter = Integer.MAX_VALUE;
        for (int i = 1; i <= Math.sqrt(areaDesired); i++) {
            if (areaDesired % i == 0) {
                if (2 * (i + areaDesired / i) < minimumPerimeter) minimumPerimeter = 2 * (i + areaDesired / i);
            }
        }
        return minimumPerimeter;
    }

}