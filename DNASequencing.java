import java.util.Scanner;

public class DNASequencing {

    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in); // Reading from System.in
        System.out.println("Enter the sequence: ");
        String sequence = reader.nextLine();
        int[] counts = getLongestArray(sequence);


        if (counts[1] == 0)
            System.out.println("A " + counts[0]);
        if (counts[1] == 1)
            System.out.println("C " + counts[0]);
        if (counts[1] == 2)
            System.out.println("G " + counts[0]);
        if (counts[1] == 3)
            System.out.println("T " + counts[0]);
    }

    public static int[] getLongestArray(String sequence) {
        int[] maxStore = new int[2];
        int[] count = new int[4];
        char[] ar = sequence.toCharArray();

        for (int i = 0; i < ar.length; i++) {
            if (i == 0) {
                if (ar[i] == 'A')
                    count[0] = 1;
                if (ar[i] == 'C')
                    count[1] = 1;
                if (ar[i] == 'G')
                    count[2] = 1;
                if (ar[i] == 'T')
                    count[3] = 1;


                for (int i1 = 0; i1 < 4; i1++) {
                    if (count[i1] == 1) {
                        maxStore[0] = 1;
                        maxStore[1] = i1;
                    }
                }


            } else {

                if (ar[i] == 'A') {
                    if (ar[i - 1] == 'A') {
                        count[0] = count[0] + 1;
                    } else {

                        count[0] = 1;
                    }
                } else if (ar[i] == 'C') {
                    if (ar[i - 1] == 'C') {
                        count[1] = count[1] + 1;
                    } else {

                        count[1] = 1;
                    }
                } else if (ar[i] == 'G') {
                    if (ar[i - 1] == 'G') {
                        count[2] = count[2] + 1;
                    } else {

                        count[2] = 1;
                    }
                } else if (ar[i] == 'T') {
                    if (ar[i - 1] == 'T') {
                        count[3] = count[3] + 1;
                    } else {

                        count[3] = 1;
                    }
                }
                for (int i1 = 0; i1 < 4; i1++) {
                    if (count[i1] > maxStore[0]) {
                        maxStore[0] = count[i1];
                        maxStore[1] = i1;
                    }

                }
                //System.out.println("Max values are Index:"+maxStore[1]+" Value:"+maxStore[0]);
            }
        }
        return maxStore;
    }

}